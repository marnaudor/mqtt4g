#include <Arduino.h>
#include <HardwareSerial.h>
#define LED_pin 22
#define pushButton_pin   15
//const uart_struct_t uart_num=2;
HardwareSerial SerialPort0(0);  //if using UART0
HardwareSerial SerialPort2(2) ; //if using UART2
char buffer[100];
int etatmqtt=0;
// put function declarations here:
int myFunction(int, int);
int compteur=0;
boolean messagerecu;
hw_timer_t *Timer0_Cfg = NULL;
 uint16_t size;
void IRAM_ATTR Timer0_ISR()
{
  //  digitalWrite(LED_pin, !digitalRead(LED_pin));
    compteur++;
}

int LectureCode(char * buf, int l);

void IRAM_ATTR toggleLED()
{
 // digitalWrite(LED_pin, !digitalRead(LED_pin));
 // compteur++;
}

void Uart_Irq(){
 // digitalWrite(LED_pin, !digitalRead(LED_pin));
   size = SerialPort0.available();
 // SerialPort0.printf("Got %d bytes on Serial to read\n", size);
 // while(SerialPort0.available()) {
    SerialPort0.read(buffer,100);
    //Serial.write(Serial.read());
 // }
  messagerecu=true;
  
}

void setup() {
  // put your setup code here, to run once:
 //  Serial.begin(115200);
 // int result = myFunction(2, 3);
  SerialPort0.begin(115200,SERIAL_8N1,3,1);

 SerialPort2.begin(115200,SERIAL_8N1,16,17);
Timer0_Cfg = timerBegin(0, 80, true);
timerAttachInterrupt(Timer0_Cfg, &Timer0_ISR, true);
timerAlarmWrite(Timer0_Cfg, 10000, true);
timerAlarmEnable(Timer0_Cfg);
messagerecu=false;
  pinMode(LED_pin, OUTPUT);
  digitalWrite(LED_pin, !digitalRead(LED_pin));
  pinMode(LED_pin, OUTPUT);
  pinMode(pushButton_pin, INPUT_PULLUP);
  attachInterrupt(pushButton_pin, toggleLED, RISING);
  //xTaskCreate(UART_ISR_ROUTINE,"uart0 interrupt",2048,NULL,5,NULL);
  SerialPort0.onReceive(Uart_Irq);
 // SerialPort0.println("ATE0");
  etatmqtt=0;
}

void loop() {
  // put your main code here, to run repeatedly:
  // digitalWrite(LED_pin, !digitalRead(LED_pin));
 // SerialPort2.printf("%f",(float)compteur);
 delay(800);
  //SerialPort0.print("AT");
 //Serial.printf("%d",(compteur),DEC);
  //SerialPort0.printf("%d",compteur,DEC);
  messagerecu=false;
    SerialPort0.println("AT+CGPSINFO");
    compteur=0;size=0;
    while ((compteur<=100)&&(!messagerecu))
      digitalWrite(LED_pin, !digitalRead(LED_pin));
    if (messagerecu==true)
      SerialPort2.println(buffer);

  switch (etatmqtt){
    case 0: messagerecu=false;
           
            SerialPort0.println("AT+CMQTTSTART");
            compteur=0;size=0;
            while ((compteur<=100)&&(!messagerecu))
              digitalWrite(LED_pin, !digitalRead(LED_pin));
            if (messagerecu==true){
              int lc=LectureCode(buffer,size);
              if (lc==0){
                SerialPort2.println("Start OK");
                etatmqtt==1;
              }
              else {
                SerialPort2.printf("erreur: %d",lc);
              }
               messagerecu=false;    
            }
            else SerialPort2.println("No Message");
            break;
  }
  
  delay(1500);
}

// put function definitions here:
int myFunction(int x, int y) {
  return x + y;
}

int LectureCode(char * buf, int l){
  int i=0;
  while((i<l)and !((buf[i]>='0')and(buf[i]<='9')))
      i++;

if (i==l)
  return -1;
return buf[i]-0x30;
}